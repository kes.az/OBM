# ÜMUMİ MƏLUMAT
Oracle Book Manager (OBM) KES Nəşrlərinin «Oracle SQL və Verilənlər Bazasının Modelləşdirilməsi. 250 Məsələ, 400 Həll» ISBN: 978-9952-464-04-7 kitabında verilən məsələləri işləmək üçün yaradılmış sistemdir. Kitabda təqdim edilən məsələləri işləmək üçün əvvəlcə kitabı qeydiyyatdan keçirmək lazımdır. Kitabı qeydiyyatdan keçirməklə OBM sistemində hesab əldə edəcəksiniz. OBM hesabla 10 şirkət və təşkilatın stabil Oracle verilənlər bazasına və hər bir oxucu üçün ayrılmış dinamik fərdi bazaya qoşulmaq mümkündür. Dinamik fərdi baza ilə kitabın hər bir orijinal oxucusuna 10 MB yer ayrılır.

# OBM LİSENZİYA MÜQAVİLƏSİ
Menyu sətrinin "Məlumat" bəndində "Lisenziya" alt menyusuna klik edin. Lisenziya müqaviləsində kitabın uğurlu qeydiyyatı üçün şərtlər və məlumat məxfiliyi haqqında öhdəliklər qeyd edilmişdir. Lisenziya müqaviləsi ilə diqqətlə tanış olun. Kitabın uğurlu qeydiyyatı üçün müqavilənin təsdiqi tələb olunur.

# OBM SİSTEMİNİN QURAŞDIRILMASI
Oracle Book Manager proqramının qrafik istifadəçi interfeysi Java 1.8 ilə yaradılmışdır. Proqramı Java mühiti olan (JRE 1.8.x və ya yuxarı versiya) istənilən əməliyyat sistemində işlətmək mümkündür. 
> Kompüterinizin əməliyyat sisteminə uyğun Java mühitini buradan endirə bilərsiniz: http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html
> Java mühitinin quraşdırılma qaydası: https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html
OBM sistemi Oracle 11g və 12c ilə test edilmişdir. Hazırda sistem Oracle XE 11.2.0.2.0 versiya ilə işləyir.

# KİTABIN QEYDİYYATI
OBM proqramı açıldıqdan sonra menyu siyahısından "Qeydiyyat" bəndini seçin. Qeydiyyat formasında ulduz (*) ilə qeyd edilən vacib sahələri daxil edin. Vacib olmayan sahələr kitab oxucularının statistikası üçün nəzərdə tutulmuşdur. Qeydiyyat zamanı tələb olunan unikal kod əldə etdiyiniz kitabda verilmişdir. Nəzərə alın ki, unikal kod yalnız və yalnız kitabı əldə etdiyiniz andan etibarən 24 saat müddətində aktiv edilir. Hər bir orijinal kitab bir unikal kodla nəşr edilmişdir. Bir unikal kodla yalnız bir orijinal kitab qeydiyyatdan keçirmək mümkündür. Kitab yalnız bir dəfə qeydiyyatdan keçirilir. Eyni bir unikal kodla kitabı təkrar qeydiyyatdan keçirmək mümkün deyil.

# OBM HESAB
Kitabı uğurla qeydiyyatdan keçirdikdən sonra OBM hesabınıza daxil ola bilərsiniz. Əsas proqramda qeydiyyat zamanı daxil etdiyiniz email və ya kitabınızın unikal kodu ilə şifrənizi daxil etməklə hesabınıza keçə bilərsiniz. Sistemə uğurlu daxil olduqdan sonra menyu sətrində "VB" (verilənlər bazası) bəndini açın. Buradan işləmək istədiyiniz Oracle bazanı seçə bilərsiniz.  "Stabil" alt menyusunda kitabda verilən şirkət və təşkilatlara aid bazalar saxlanılmışdır. Stabil bazalarla yalnız və yalnız SELECT əməliyyatları etmək mümkündür. Əgər siz cədvəl yaratmaq (CREATE TABLE...), cədvələ məlumat yazmaq (INSERT INTO...), cədvəldən məlumat silmək (DELETE FROM...), cədvəli silmək (DROP TABLE…) və digər SQL kodları icra etmək istəsəniz, o zaman "Dinamik" alt menyusundan "Fərdi Oracle Sxemim" bazasını seçməyiniz kifayətdir. Seçdiyiniz bazanın strukturunu görmək üçün alətlər panelində "Sxemi Göstər" (Alt+S) əmrini işə salmaq lazımdır. Əgər sizi hər hansı bir cədvəlin və ya görünüşün strukturu, məsələn, sütun tipi, sütun nömrəsi və s. maraqlandırarsa, o zaman alətlər panelində verilmiş mətn sahəsinə qoşulduğunuz bazada mövcud olan cədvəl və ya görünüşün adı daxil edilməli və "Cədvəl və ya Görünüşün Strukturunu Göstər" (Alt+A) düyməsi klik edilməlidir. Stabil bazalarla işləməmişdən əvvəl tövsiyə olunur ki, bazanın kitabda verilən mətn təsviri, ER diaqram və SQL DDL strukturu ilə yaxşı tanış olasınız.
Bir hesabla eyni zamanda yalnız bir istifadəçi – orijinal kitabın sahibi Oracle Book Manager sisteminə daxil ola bilər. İkinci və növbəti paralel sessiyalar məhdudlaşdırılmışdır və lisenziya şərtləri daxilində OBM server avtomatik müşahidə edir. OBM lisenziya şərtləri ilə hesab məlumatlarının paylaşılmasına icazə verilmir.

# ƏLAVƏ PROQRAMLAR
Kitabdakı məsələləri işləmək üçün OBM-dən başqa heç bir sistem tələb olunmur. Amma verilən məsələləri daha zövqlü və gözlənilən formada həll etmək üçün 2 əlavə proqram məsləhət görülür:

> Verilənlər bazasının modelləşdirilməsinə aid olan məsələləri işləmək üçün ER diaqramları dəstəkləyən proqramlardan istifadə edə bilərsiniz. Kitabdakı ER diaqramlar Dia (www.dia-installer.de) sistemi ilə modelləşdirilmişdir. Alternativ olaraq yEd, MS Visio və s. sistemlərdən də istifadə edə bilərsiniz. 
> CD-də olan həll fayllarını hər hansı bir redaktor proqramının köməyilə açmaq mümkündür. Kitabdakı həllər Notepad++ (www.notepad-plus-plus.org) proqramından istifadə edilməklə hazırlanmışdır. Buna baxmayaraq SQL faylları kompüterinizdə olan hər hansı bir sadə redaktor proqramı ilə də aça bilərsiniz. Məsələn, Windows platformasında Notepad proqramı ilə.

# CD HƏLLƏRİN İŞLƏDİLMƏSİ
OBM hesaba daxil olduqdan sonra işləyəcəyiniz şirkət və ya təşkilatın bazasını seçin. Bu zaman aktiv proqram pəncərəsinin başlığında qoşulduğunuz bazanın adını görəcəksiniz. Alətlər panelindən "Sxemi Göstər" əmri ilə qoşulduğunuz bazanın obyektləri ilə tanış olun. Kitabda verilən məsələnin həllini "SQL İş Sahəsi" bölməsinə yazın və sorğunu icra edin (Alt+Enter). Məsələnin təklif olunan həllərini kitabla birlikdə əldə etdiyiniz CD-nin uyğun qovluğundan əldə edə bilərsiniz. Düzgün cavabları icra etməklə öz həllinizin doğru olub-olmadığını yoxlaya bilərsiniz. OBM-in SQL iş sahəsi kursor ilə seçilmiş mətnin icra olunmasını da dəstəkləyir. Bunun üçün icra edəcəyiniz sorğunu nişanlayıb alətlər panelindən "İcra Et" (Alt+Enter) əmrini işə salmaq lazımdır.
CD-dəki həll fayllarına aşağıdakı konvensiya ilə ad verilmişdir: 

<şirkət və ya təşkilatın adı>_<məsələnin nömrəsi>[_<məsələnin bəndi>][a<alternativ həll nömrəsi>].sql

Məsələn, chocolate_factory_12_2a1.sql. Bu həll "Şokolad Fabriki" adlı şirkətin 12-ci məsələsinin 2-ci bəndinin 1-ci alternativ həllidir.

Kitabda verilən məsələlərlə yanaşı stabil bazada verilən məlumatlara əsasən özünüzə məxsus müxtəlif hesabatlar da hazırlaya bilər, dinamik baza ilə SQL biliklərinizi hərtərəfli təcrübəyə keçirə bilərsiniz.

# ŞİFRƏNİN BƏRPASI
Əgər OBM hesabınızın şifrəsini unutmusunuzsa, o zaman qeydiyyatdan keçərkən daxil etdiyiniz email ilə books@kes.az ünvanına kitabınızın unikal kodu ilə birlikdə «Şifrəmi unutdum:unikal kod» başlığında sorğu göndərin. Daxil olan sorğular ardıcıllıqla 24 saat müddətində cavablandırılır. Nəzərə alın ki, eyni və təkrar sorğular cavablandırmanı gecikdirə bilər.

# TEXNİKİ PROBLEMLƏR
Kitabın onlayn sifarişi, qeydiyyatı və ya OBM hesaba daxil olarkən qarşılaşdığınız texniki problemlər aşağıdakı səbəblərə görə ola bilər:
> İnternet əlaqəniz mövcud deyil
> IP ünvanınız proksi ilə məhdudlaşdırılmışdır
> Antivirus sistemi aktivdir və qoşulmaya mane olur
> Kompüterinizdə Java mühiti (JRE) quraşdırılmayıb və ya 1.8 versiyadan aşağıdır
> Daxil etdiyiniz məlumatlar yanlışdır
Qeyd edilən səbəbləri yoxlayın. Əgər qarşılaşdığınız texniki problem qeyd edilən bəndlərdən birinə uyğun deyilsə, o zaman qeydiyyatdan keçərkən daxil etdiyiniz email ilə books@kes.az ünvanına kitabınızın unikal kodu ilə birlikdə «Texniki problem:unikal kod» başlığında sorğu göndərin. Daxil olan sorğular ardıcıllıqla 24 saat müddətində cavablandırılır. Nəzərə alın ki, eyni və təkrar sorğular cavablandırmanı gecikdirə bilər. Kitabda verilən məsələlərin həlli zamanı qarşılaşdığınız spesifik problemlər, məsələn, SQL sorğunun nəticəsi, Oracle RDBMS lokal kompüterə quraşdırılması və s. texniki problemlərə aid deyil. Bu tip suallar kitaba aid forumda müzakirə oluna bilər. Foruma daxil olmaq üçün alətlər lövhəsindəki sual işarəsini klik (Alt+F) edin.

# ORACLE SQL FORUM
Kitabda verilən məsələlərin həllində qarşılaşdığınız sualları kitaba aid forumda verə bilərsiniz. Foruma daxil olmaq üçün alətlər lövhəsindəki sual işarəsini klik (Alt+F) edin.

# OBM YENİ VERSİYA
OBM sistemi hal-hazırda versiya (1.4) təqdim edilir. Müəyyən müddətdən sonra sistemə yeni imkanlar və alətlər daxil edilə bilər. Sistemin ən son versiyasını www.kes.az saytından əldə edə bilərsiniz.

# İT TƏLİMLƏR
Kitabdakı məsələləri işləməkdə çətinliyiniz yaranarsa və ya öyrənmək üçün sizi İT-nin hər hansı bir sahəsi maraqlandırarsa, KES İT təlimlərinə müraciət edə bilərsiniz. Ətraflı "KES" menyusuna baxın.

# İT NƏŞRLƏR
KES Nəşrləri Azərbaycanın ilk İT və texniki ədəbiyyatlar üzrə ixtisaslaşmış nəşriyyatıdır. Yeni nəşrləri www.kes.az saytından izləyə bilərsiniz. 

